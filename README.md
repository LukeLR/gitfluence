# gitfluence

Small Python script to export a Confluence wiki page (including history!) as markdown into a git repository. Version history content and metadata like authorship, author date and edit descriptions are translated into git history. Uses the [Confluence Python API](https://github.com/atlassian-api/atlassian-python-api) and [markdownify](https://github.com/matthewwithanm/python-markdownify).

## Requirements

Simply install the two dependencies, e.g. via pip (recommended):

```
pip install --user markdownify atlassian-python-api
```

## Setup

Just clone this repository or download the zip and extract it. `cd` into that directory and run `python gitfluence.py` or add the directory to your `PATH` environment variable and call `gitfluence.py` from whereever you want.

## Usage

By running `gitfluence -h`, an explaination for all parameters will appear:

```
gitfluence$ python gitfluence.py -h
usage: gitfluence.py [-h] -i INSTANCE -u USERNAME -p PASSWORD -d DESTINATION -s SPACE -t TITLE -f FILENAME

Command line arguments for extracting Confluence page history into a git repository

options:
  -h, --help            show this help message and exit
  -i INSTANCE, --instance INSTANCE
                        URL of the Confluence instance to use, e.g. https://wiki.example.com
  -u USERNAME, --username USERNAME
                        Confluence username
  -p PASSWORD, --password PASSWORD
                        Confluence password
  -d DESTINATION, --destination DESTINATION
                        Directory of the git repository
  -s SPACE, --space SPACE
                        Key of the confluence space
  -t TITLE, --title TITLE
                        Title of the page to extract
  -f FILENAME, --filename FILENAME
                        Filename for the exported file, should end in .md
```

### Example

A simple usage example might look like this:

```
python gitfluence.py -i https://wiki.example.com -u myuser -p "my password" -d ~/git/myrepository -s MYSPACE -t "Example page" -f examplePage.md
```

## Contributions

Contributions and feedback of any kind, be it reporting bugs, proposing new features, filing merge requests or taking part in the discussion is very encouraged and appreciated. Let me know what you think!

## ToDo

As this is just a small side project, it's very basic and there's a lot to improve. At the moment, it covers my personal needs, but in the future, additional features might be added if I (or someone else) finds the time. This might include:

- Check support for page attachments, especially images
- Support more Confluence makros
- Export page discussion
- Export page metadata and raw data
- Styling of Confluence makros
- General styling
- ... (add yours!)
