#!/usr/bin/python3

import argparse
import os
import subprocess
from atlassian import Confluence
from markdownify import MarkdownConverter
from atlassian.errors import ApiError

def parse_args():
	parser = argparse.ArgumentParser(description = 'Command line arguments for extracting Confluence page history into a git repository')
	
	parser.add_argument('-i', '--instance', type=str, help="URL of the Confluence instance to use, e.g. https://wiki.example.com", required=True)
	parser.add_argument('-u', '--username', type=str, help="Confluence username", required=True)
	parser.add_argument('-p', '--password', type=str, help="Confluence password", required=True)
	parser.add_argument('-d', '--destination', type=str, help="Directory of the git repository", required=True)
	parser.add_argument('-s', '--space', type=str, help="Key of the confluence space", required=True)
	parser.add_argument('-t', '--title', type=str, help="Title of the page to extract", required=True)
	parser.add_argument('-f', '--filename', type=str, help="Filename for the exported file, should end in .md", required=True)
	
	return parser.parse_args()

class AtlassianConverter(MarkdownConverter):
	def __init__(self, **options):
		super().__init__(**options)
		setattr(self, 'convert_ac:structured-macro', self.convert_ac_structured_macro)
		setattr(self, 'convert_ac:parameter', self.convert_ac_parameter)
		setattr(self, 'convert_ac:plain-text-body', self.convert_ac_plain_text_body)
	
	def convert_ac_structured_macro(self, el, text, convert_as_inline):
		return text
	
	def convert_ac_parameter(self, el, text, convert_as_inline):
		if el.attrs['ac:name'] == 'title':
			return self.convert_h4(el, text, convert_as_inline)
		return ''
	
	def convert_ac_plain_text_body(self, el, text, convert_as_inline):
		if el.parent.attrs['ac:name'] == 'code':
			return self.convert_p(el, self.convert_code(el, text, convert_as_inline), convert_as_inline)
		else:
			return self.convert_p(el, text, convert_as_inline)

def main():
	args = parse_args()
	
	confluence = Confluence(url = args.instance, username = args.username, password = args.password)
	page_id = confluence.get_page_id(args.space, args.title)
	
	os.chdir(args.destination)
	
	try:
		i = 0
		while True:
			i += 1
			page = confluence.get_page_by_id(page_id, version=i, expand="body.storage,version")
			markdown = AtlassianConverter().convert(page['body']['storage']['value'])
			message = "gitfluence: " + page['version']['message']
			author_username = page['version']['by']['username']
			author_displayname = page['version']['by']['displayName']
			timestamp = page['version']['when']
			
			with open(args.filename, 'w') as f:
				f.write(markdown)
			
			subprocess.run(["git", "add", args.filename], check=True)
			subprocess.run(["git", "commit", args.filename, "-m", message, f"--author='{author_displayname} <{author_username}>'", f"--date='{timestamp}'"])
			
	except ApiError as e:
		pass

if __name__ == '__main__':
	main()
